import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicSplitPaneComponent } from './dynamic-split-pane.component';

describe('DynamicSplitPaneComponent', () => {
  let component: DynamicSplitPaneComponent;
  let fixture: ComponentFixture<DynamicSplitPaneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DynamicSplitPaneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicSplitPaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
