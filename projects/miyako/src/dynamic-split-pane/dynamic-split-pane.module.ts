import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { DynamicSplitPaneComponent } from './dynamic-split-pane.component';

@NgModule({
  declarations: [
    DynamicSplitPaneComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    DynamicSplitPaneComponent
  ]
})
export class DynamicSplitPaneModule { }
