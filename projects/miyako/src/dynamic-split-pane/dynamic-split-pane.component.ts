import {Component, Input, OnInit, TemplateRef} from '@angular/core';

@Component({
  selector: 'miyako-dynamic-split-pane',
  templateUrl: './dynamic-split-pane.component.html',
  styleUrls: ['./dynamic-split-pane.component.scss']
})
export class DynamicSplitPaneComponent implements OnInit {


  @Input()
  sidePanel: TemplateRef<any>;

  @Input()
  contentPanel: TemplateRef<any>;

  @Input()
  panelAlignment: 'start' | 'end' = 'start';

  @Input()
  hasHeader = true;

  @Input()
  maxSideWidth: string = '300px';

  constructor() { }

  ngOnInit(): void {
  }

}
