import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserMenuComponent } from './components/user-menu/user-menu.component';
import {UserInfoStore} from './data/user-info.store';
import {IonicModule} from '@ionic/angular';
import { LoginPageOutletComponent } from './components/login-page-outlet/login-page-outlet.component';
import { LoginPageComponent } from './components/login-page/login-page.component';

import {SmallCenteredPanelLayoutModule} from '../layouts/small-centered-panel-layout';
import { SignUpPageComponent } from './components/sign-up-page/sign-up-page.component';



@NgModule({
  declarations: [
    UserMenuComponent,
    LoginPageOutletComponent,
    LoginPageComponent,
    SignUpPageComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    SmallCenteredPanelLayoutModule
  ],
  providers: [
    UserInfoStore
  ],
  exports: [
    UserMenuComponent,
    LoginPageComponent,
    LoginPageOutletComponent,
    SignUpPageComponent
  ]
})
export class MiyakoUserModule { }
