export interface UserInfo {
  username: string;
  fullName: string;
  permissions: { [name: string]: boolean };
  properties: { [name: string]: any };
}
