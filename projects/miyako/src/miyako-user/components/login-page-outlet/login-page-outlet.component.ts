import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';

@Component({
  selector: 'miyako-login-page-outlet',
  templateUrl: './login-page-outlet.component.html',
  styleUrls: ['./login-page-outlet.component.css']
})
export class LoginPageOutletComponent implements OnInit {

  @ViewChild('login')
  loginTemplate: TemplateRef<any>;

  @ViewChild('content')
  contentTemplate: TemplateRef<any>;

  @ViewChild('signUp')
  signUpTemplate: TemplateRef<any>;

  constructor() { }

  ngOnInit(): void {
  }

  getActiveTemplate() {
    return this.loginTemplate;
  }
}
