import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginPageOutletComponent } from './login-page-outlet.component';

describe('LoginPageOutletComponent', () => {
  let component: LoginPageOutletComponent;
  let fixture: ComponentFixture<LoginPageOutletComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoginPageOutletComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginPageOutletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
