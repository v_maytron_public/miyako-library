import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {UserInfoStore} from '../../data/user-info.store';

@Component({
  selector: 'miyako-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.css']
})
export class UserMenuComponent implements OnInit {

  @Output()
  loginButtonPressed: EventEmitter<void> = new EventEmitter<void>();

  @Output()
  logoutButtonPressed: EventEmitter<void> = new EventEmitter<void>();

  @Output()
  signUpButtonPressed: EventEmitter<void> = new EventEmitter<void>();

  constructor(public store: UserInfoStore) { }

  ngOnInit(): void {
  }

  handleSignUp() {
    this.signUpButtonPressed.emit();
  }

  handleLogin() {
    this.loginButtonPressed.emit();
  }

  handleLogout() {
    this.logoutButtonPressed.emit();
  }
}
