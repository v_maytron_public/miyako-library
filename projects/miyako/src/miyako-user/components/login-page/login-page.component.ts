import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'miyako-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  public numbers = Array(100).fill(0).map((x,i) => i);

  constructor() { }

  ngOnInit(): void {
  }

}
