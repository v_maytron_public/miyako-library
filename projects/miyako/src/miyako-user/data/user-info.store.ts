import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {UserInfo} from '../models/user-info';
import {filter, first, map, take} from 'rxjs/operators';

@Injectable()
export class UserInfoStore {

  private userInfoSubject: BehaviorSubject<UserInfo> = new BehaviorSubject(null);

  public userInfo: Observable<UserInfo> = this.userInfoSubject.asObservable().pipe(filter(value => null !== value));
  public hasUserInfo: Observable<boolean> = this.userInfoSubject.asObservable().pipe(map(value => value !== null));

  public init(userInfo: UserInfo): void {
    this.userInfoSubject.next(userInfo);
  }

  public removeUserInfo(): void {
    this.userInfoSubject.next(null);
  }

  public getUserInfo(): Observable<UserInfo> {
    return this.userInfo.pipe(first());
  }

}
