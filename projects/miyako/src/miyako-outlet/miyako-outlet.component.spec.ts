import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MiyakoOutletComponent } from './miyako-outlet.component';

describe('MiyakoOutletComponent', () => {
  let component: MiyakoOutletComponent;
  let fixture: ComponentFixture<MiyakoOutletComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MiyakoOutletComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MiyakoOutletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
