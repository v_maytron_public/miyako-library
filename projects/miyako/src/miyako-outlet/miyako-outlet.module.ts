import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MiyakoOutletComponent } from './miyako-outlet.component';
import { IonicModule } from '@ionic/angular';



@NgModule({
  declarations: [
    MiyakoOutletComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    MiyakoOutletComponent
  ]
})
export class MiyakoOutletModule { }
