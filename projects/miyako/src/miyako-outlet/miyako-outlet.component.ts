import {Component, Input, OnInit, TemplateRef} from '@angular/core';

@Component({
  selector: 'miyako-outlet',
  templateUrl: './miyako-outlet.component.html',
  styleUrls: ['./miyako-outlet.component.scss']
})
export class MiyakoOutletComponent implements OnInit {

  @Input()
  title = 'Miyako';

  @Input()
  toolbarContentRight: TemplateRef<any>;

  constructor() { }

  ngOnInit(): void {
  }

}
