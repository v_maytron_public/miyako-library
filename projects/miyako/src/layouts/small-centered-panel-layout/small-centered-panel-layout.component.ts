import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'miyako-small-centered-panel-layout',
  templateUrl: './small-centered-panel-layout.component.html',
  styleUrls: ['./small-centered-panel-layout.component.scss']
})
export class SmallCenteredPanelLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
