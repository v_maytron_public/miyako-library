import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SmallCenteredPanelLayoutComponent } from './small-centered-panel-layout.component';

describe('SmallCenteredPanelLayoutComponent', () => {
  let component: SmallCenteredPanelLayoutComponent;
  let fixture: ComponentFixture<SmallCenteredPanelLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SmallCenteredPanelLayoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SmallCenteredPanelLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
