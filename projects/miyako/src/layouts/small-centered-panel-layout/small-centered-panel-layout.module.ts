import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SmallCenteredPanelLayoutComponent } from './small-centered-panel-layout.component';
import {IonicModule} from '@ionic/angular';



@NgModule({
  declarations: [
    SmallCenteredPanelLayoutComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    SmallCenteredPanelLayoutComponent
  ]
})
export class SmallCenteredPanelLayoutModule { }
